const path = require("path");
const boards = require(path.join(__dirname, "jsonFiles", "boards.json"));

module.exports = (id = "mcu453ed", cb) => {
  setTimeout(() => {
    let index = Object.entries(boards).findIndex(() => id);
    cb(id, boards[index].name);
  }, 2000);
};
