const path = require("path");
const cards = require(path.join(__dirname, "jsonFiles", "cards.json"));

module.exports = (id = "qwsa221", cb) => {
  setTimeout(() => {
    if (typeof id == "string") {
      cb(cards[id]);
    } else {
      id.forEach((id) => cb(cards[id]));
    }
  }, 2000);
};
