const path = require("path");
const lists = require(path.join(__dirname, "jsonFiles", "lists.json"));

module.exports = (id = "mcu453ed", cb) => {
  setTimeout(() => {
    cb(lists[id]);
  }, 2000);
};
