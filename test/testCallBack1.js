//Problem 1: Write a function that will return a particular board's information based on the boardID
//that is passed from the given list of boards in boards.json and then pass control back to the code
//that called it by using a callback function.

const path = require("path");
const getBoardInfo = require(path.join("..", "callBack1.js"));

try {
  getBoardInfo(process.argv[2], (id, name) => {
    console.log({ id, name });
  });
} catch (err) {
  console.log(err);
}
