const path = require("path");
const boards = require(path.join(__dirname, "jsonFiles", "boards.json"));
const boardsInfo = require(path.join(__dirname, "callBack1.js"));
const listInfo = require(path.join(__dirname, "callBack2.js"));
const cardsInfo = require(path.join(__dirname, "callBack3.js"));

module.exports = (id) => {
  boardsInfo(id, (id, name) => {
    console.log({ id, name });
    listInfo(id, (lists) => {
      console.log(lists);
      let idList = lists.reduce((idList, list) => {
        if (list.name == "Mind" || list.name == "Space") idList.push(list.id);
        return idList;
      }, []);
      cardsInfo(idList, (cards) => {
        console.log(cards);
      });
    });
  });
};
