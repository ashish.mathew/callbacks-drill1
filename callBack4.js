const path = require("path");
const boards = require(path.join(__dirname, "jsonFiles", "boards.json"));
const boardsInfo = require(path.join(__dirname, "callBack1.js"));
const listInfo = require(path.join(__dirname, "callBack2.js"));
const cardsInfo = require(path.join(__dirname, "callBack3.js"));

module.exports = (id) => {
  boardsInfo(id, (id, name) => {
    console.log({ id, name });
    listInfo(id, (lists) => {
      console.log(lists);
      let index = lists.findIndex((item) => (item.name = "Mind"));
      cardsInfo(lists[index].id, (cards) => {
        console.log(cards);
      });
    });
  });
};
